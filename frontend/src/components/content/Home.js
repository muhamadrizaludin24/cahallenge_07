
import '../template/style.css';
import Header from '../template/Header';
import OurServices from './subcontent/OurServices';
import WhyUs from './subcontent/WhyUs';
import Testimonial from './subcontent/Testimonial';
import SewaMobil from './subcontent/SewaMobil';
import FAQ from './subcontent/FAQ';

const Home = () => {
  return (
        <div>
            <Header />
            <OurServices />
            <WhyUs />
            <Testimonial />
            <SewaMobil />
            <FAQ />
        </div>
    )
}

export default Home