import testi_img_1 from '../../images/testi-img-1.png';
import testi_img_2 from '../../images/testi-img-2.png';
import left_arrow from '../../images/left-arrow.png';
import right_arrow from '../../images/right-arrow.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import '../../template/style.css';

const Testimonial = () => {
    const element = <FontAwesomeIcon icon={faStar} />
    
    
    
  return (
    // <!--Content Testimonial-->
    <section id="Testimonial" className="testimonial">
      <div className="container">
        <h1 className="section-header">Testimonial</h1>
        <p className="lead text-center">Berbagai review positif dari para pelanggan kami</p>
        <div className="testimonial-view">
          <div className="carousel slide" id="testimonialCarousel" data-bs-ride="carousel">
            <div className="carousel-inner" style={{paddingBottom:"30px"}}>
              <div className="carousel-item active" style={{marginBottom:"20px", marginTop:"-30px"}}>
                <div className="block shadow-sm">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="user">
                        <div className="image">
                          <img src={testi_img_1} alt="client-1" />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <div className="content">
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <p className="pt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, 
                          consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item" style={{marginBottom:"20px", marginTop:"-30px"}}>
                <div className="block shadow-sm">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="user">
                        <div className="image">
                          <img src={testi_img_2} alt="client-1" />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <div className="content">
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <p className="pt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, 
                          consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item" style={{marginBottom:"20px", marginTop:"-30px"}}>
                <div className="block shadow-sm">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="user">
                        <div className="image">
                          <img src={testi_img_1} alt="client-1" />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <div className="content">
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <i>{element}</i>
                        <p className="pt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, 
                          consectetur adipiscing elit, 
                          sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                          sed do eiusmod”
                        </p>
                        <strong>John Dee 32, Bromo</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button className="carousel-control-prev justify-content-center"  type="button" data-bs-target="#testimonialCarousel" data-bs-slide="prev">
                <img src={left_arrow} alt="left-arrow" />
                <span className="visually-hidden">Previous</span>
              </button>
              <button className="carousel-control-next justify-content-center" style={{textAlign:"center"}} type="button" data-bs-target="#testimonialCarousel" data-bs-slide="next">
                <img src={right_arrow} alt="right-arrow" />
                <span className="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Testimonial